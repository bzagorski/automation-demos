package com.example.groovy_selenide

import com.codeborne.selenide.Configuration
import com.codeborne.selenide.Selenide
import com.codeborne.selenide.logevents.SelenideLogger
import io.qameta.allure.selenide.AllureSelenide
import org.junit.jupiter.api.*
import static org.junit.jupiter.api.Assertions.*

import static com.codeborne.selenide.Condition.attribute
import static com.codeborne.selenide.Condition.visible
import static com.codeborne.selenide.Selenide.*

class MainPageTest {
    MainPage mainPage = new MainPage()

    @BeforeAll
    static void setUpAll() {
        Configuration.browserSize = "1280x800"
        Configuration.browser = "firefox"
        SelenideLogger.addListener("allure", new AllureSelenide())
    }

    @BeforeEach
    void setUp() {
        open("https://www.jetbrains.com/")
    }

    @Test
    void search() {
        mainPage.searchButton.click()

        $("#header-search").sendKeys("Selenium")
        $x("//button[@type='submit' and text()='Search']").click()

        $(".js-search-input").shouldHave(attribute("value", "Selenium"))
    }

    @Test
    void toolsMenu() {
        mainPage.toolsMenu.hover()

        $(".menu-main__popup-wrapper").shouldBe(visible)
    }

    @Test
    void navigationToAllTools() {
        mainPage.seeAllToolsButton.click()

        $(".products-list").shouldBe(visible)
        assertEquals("All Developer Tools and Products by JetBrains", Selenide.title())
    }
}
