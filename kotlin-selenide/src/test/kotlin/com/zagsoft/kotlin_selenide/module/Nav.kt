package com.zagsoft.kotlin_selenide.module

import com.codeborne.selenide.Selenide
import com.codeborne.selenide.Selenide.element

class Nav {
    val root = element(".primary_header")
    val burgerButton = element("#react-burger-menu-btn")
}