package com.zagsoft.kotlin_selenide.module

import com.codeborne.selenide.Selenide

class Header {

    private val root = Selenide.element(".header_secondary_container")
    val title = root.find(".title")
    val productSortSelect = root.find(".product_sort_container")
    val backToProductsButton = root.find("#back-to-products")
}