package com.zagsoft.kotlin_selenide

import com.codeborne.selenide.Condition
import com.codeborne.selenide.Configuration
import com.codeborne.selenide.Selenide.element
import com.codeborne.selenide.Selenide.open
import com.zagsoft.kotlin_selenide.page.LoginPage
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class LoginPageTest {

    companion object {

        lateinit var loginPage : LoginPage

        @BeforeAll
        @JvmStatic
        fun setupAll() {
            loginPage = LoginPage()
            Configuration.browser = "firefox"
            Configuration.baseUrl = "https://www.saucedemo.com/"
            open("")
        }
    }

    @Test
    @DisplayName("Login as standard user")
    fun loginAsStandardUser() {
        loginPage.at().login("standard_user", "secret_sauce")
        element("#shopping_cart_container").shouldBe(Condition.visible)
    }

    @Test
    @DisplayName("Login as locked out user")
    fun loginAsLockedOutUser() {
        loginPage.at().login(LoginPage.UserType.LOCKED_OUT_USER)

        loginPage.errorMessage
            .shouldHave(Condition.text("Epic sadface: Sorry, this user has been locked out."))
        loginPage.closeErrorMessage()
                .errorMessageButton
                .shouldNotBe(Condition.visible)
    }

    @Test
    @DisplayName("Login as problem user")
    fun loginAsProblemUser() {

    }

    @Test
    @DisplayName("Login as performance user")
    fun loginAsPerformanceUser() {

    }

}