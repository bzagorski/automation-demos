package com.zagsoft.kotlin_selenide.page

import com.codeborne.selenide.Condition
import com.codeborne.selenide.Selenide
import com.codeborne.selenide.Selenide.element

class LoginPage {
    val usernameInput = element("#user-name")
    val passwordInput = element("#password")
    val loginButton = element("#login-button")
    val loginLogo = element(".login_logo")
    val errorMessage = element(".error-message-container")
    val errorMessageButton = element("button.error-button")

    fun at() : LoginPage {
        loginLogo.shouldBe(Condition.visible)
        return this
    }

    fun login(username: String, password: String) {
        usernameInput.sendKeys(username)
        passwordInput.sendKeys(password)
        loginButton.click()
    }

    fun login(user : UserType) {
        login(user.getUserName(), user.getPassword())
    }

    fun getErrorMessage() : String {
        return errorMessage.text
    }

    fun closeErrorMessage() : LoginPage {
        errorMessageButton.shouldBe(Condition.visible)
                .click()
        return this
    }

    enum class UserType {
        STANDARD_USER,
        LOCKED_OUT_USER,
        PROBLEM_USER,
        PERFORMANCE_GLITCH_USER;

        fun getUserName() : String {
            return name.toLowerCase()
        }

        fun getPassword() : String {
            return "secret_sauce"
        }
    }

}